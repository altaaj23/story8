from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    response ={}
    return render(request,'base.html',response)
