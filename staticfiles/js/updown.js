$(function() {
    $('.up').on('click', function(e) {
      var wrapper = $(this).closest('.accordion-section')
      wrapper.insertBefore(wrapper.prev())
    })

    $('.down').on('click', function(e) {
      var wrapper = $(this).closest('.accordion-section')
      wrapper.insertAfter(wrapper.next())
    })
  })
  